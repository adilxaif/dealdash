﻿using DealDash.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DealDash.Web.ViewModels
{
    public class AuctionsListingViewModel : PageViewModel
    {
        public int? pageNO { get; set; }
        public List<Category> Categories { get; set; }
        public List<Auction> Auctions { get; set; }
        public int? CategoryID { get; set; }
        public string searchTerm { get; set; }
       
        public Pager Pager { get; set; }

    }
    public class AuctionViewModel:PageViewModel
    {
        public List<Auction> AllAuctions { get; set; }
        public List<Auction> PromotedAuctions { get; set; }
    }

    public class AuctionDetailViewModel : PageViewModel
    {
        public Auction Auction { get; set; }
    }

    public class CreateAuctionViewModel : PageViewModel
    {
        public int CategoryID { get; set; }
        [Required]
        [MinLength(4, ErrorMessage = "Minimum length should be 4")]
        [MaxLength(8, ErrorMessage = ("Max should be 8"))]
        public string Title { get; set; }

        public string Description { get; set; }
        [Range(5, 1000000, ErrorMessage = "Range between 5 and 1000000")]
        public decimal ActualAmount { get; set; }
        public DateTime? StartingTime { get; set; }
        public DateTime? EndingTime { get; set; }
        public string AuctionPictures { get; set; }
        public List<Category> Categories { get; set; }
        public int ID { get; set; }
        public List<AuctionPicture> AuctionPicturesList { get; set; }
    }
}