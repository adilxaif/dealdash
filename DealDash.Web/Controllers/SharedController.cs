﻿using DealDash.Entities;
using DealDash.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DealDash.Web.Controllers
{
    public class SharedController : Controller
    {
        [HttpPost]
        public JsonResult UploadPicture()
        {
            JsonResult json = new JsonResult();
            List<object> picturejson = new List<object>();
            var pictures = Request.Files;
            for (int i = 0; i < pictures.Count; i++)
            {
                var picture = pictures[i];
                var filename =Guid.NewGuid()+ Path.GetExtension(picture.FileName);
                var path = Server.MapPath("~/Content/images/") + filename;
                picture.SaveAs(path);

                var dbpicture = new Picture();//we need url back to see in div
                dbpicture.URL = filename;//to save in picture table
                int picID = SharedService.Instance.SaveAuctionPic(dbpicture);//id return

                picturejson.Add(new { ID = picID, pictureurl = filename });//retrn the picIDs and url to see pic in div
            }
            json.Data = picturejson;//return all ids and url of all pic
            return json;
        }
    }
}