﻿using DealDash.Entities;
using DealDash.Services;
using DealDash.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DealDash.Web.Controllers
{
    public class AuctionController : Controller
    {
        // GET: Auction
        public ActionResult Index(int? categoryID, string searchTerm, int? pageNO)
        {
            AuctionsListingViewModel model = new AuctionsListingViewModel();
            model.PageTitle = "Auctions";
            model.PageDescription = "This is Auctions Page";
            model.CategoryID = categoryID;
            model.searchTerm = searchTerm;
            model.pageNO = pageNO;
            model.Categories = CategoriesService.Instance.GetAllCategory();

           return View(model);
                 
        }
        // GET: Auction
        public ActionResult Listing(int?categoryID,string searchTerm,int?pageNO)
        {
            AuctionsListingViewModel model = new AuctionsListingViewModel();
            //  model.pageNO = pageNO ?? 1;
            var pageSize = 2;
            model.Auctions = AuctionService.Instance.SearchAuction(categoryID,searchTerm,pageNO, pageSize);
            int totalcount = AuctionService.Instance.getAuctionCount(categoryID, searchTerm);
            model.Pager = new Pager(totalcount, pageNO, pageSize);
            
           return PartialView(model);
            
          
        }

        public ActionResult Create()
        {
            CreateAuctionViewModel model = new CreateAuctionViewModel();
            model.Categories = CategoriesService.Instance.GetAllCategory();
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Create(CreateAuctionViewModel model)
        {
            JsonResult result = new JsonResult();
            if (ModelState.IsValid)
            {
                Auction auction = new Auction();
                auction.CategoryID = model.CategoryID;
                auction.Title = model.Title;
                auction.Description = model.Description;
                auction.ActualAmount = model.ActualAmount;
                auction.StartingTime = model.StartingTime;
                auction.EndingTime = model.EndingTime;
                //check if we have auctionpicid posted back from form
                if (!string.IsNullOrEmpty(model.AuctionPictures))
                {
                    var PicIDS = model.AuctionPictures.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(ID => int.Parse(ID)).ToList();

                    auction.AuctionPictures = new List<AuctionPicture>();
                    auction.AuctionPictures.AddRange(PicIDS.Select(x => new AuctionPicture() { PictureID = x }).ToList());//to save in auctionpicture table with auction id and pic id

                }   //foreach (var picID in PicIDS)
                    //{
                    //    var auctionpic = new AuctionPicture();
                    //    auctionpic.PictureID = picID;«
                    //    auction.AuctionPictures.Add(auctionpic);
                    //}

                AuctionService.Instance.SaveAuction(auction);
                result.Data = new { success = true };
            }
            else
            {
                result.Data = new { success = false, Error = "Please Enter Valid Value" };
            }
            return result;
        }

        public ActionResult Edit(int ID)
        {
            CreateAuctionViewModel model = new CreateAuctionViewModel();
       
            var auction = AuctionService.Instance.AuctionGetById(ID);
            model.ID = auction.ID;
            model.Title = auction.Title;
            model.CategoryID = auction.CategoryID;
            model.Description = auction.Description;
            model.ActualAmount = auction.ActualAmount;
            model.StartingTime = auction.StartingTime;
            model.EndingTime = auction.EndingTime;
            model.AuctionPicturesList = auction.AuctionPictures;

            model.Categories = CategoriesService.Instance.GetAllCategory();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Edit(CreateAuctionViewModel model)
        {
            Auction auction =new Auction();
            auction.ID = model.ID;
            auction.CategoryID = model.CategoryID;
            auction.Title = model.Title;
            auction.Description = model.Description;
            auction.ActualAmount = model.ActualAmount;
            auction.StartingTime = model.StartingTime;
            auction.EndingTime = model.EndingTime;
            if (!string.IsNullOrEmpty(model.AuctionPictures))
            {
                var PicIDS = model.AuctionPictures.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(ID => int.Parse(ID)).ToList();

                auction.AuctionPictures = new List<AuctionPicture>();
                auction.AuctionPictures.AddRange(PicIDS.Select(x => new AuctionPicture() {AuctionID=auction.ID, PictureID = x }).ToList());//to save in auctionpicture table with auction id and pic id

            }

            AuctionService.Instance.UpdateAuction(auction);
            return RedirectToAction("Listing");
         
        }

        public ActionResult Delete(int ID)
        {
            var model = AuctionService.Instance.AuctionGetById(ID);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Delete(Auction auction)
        {
            AuctionService.Instance.AuctionDelete(auction);
            return RedirectToAction("Listing");
        }

        
        public ActionResult Details(int id)
        {
            AuctionDetailViewModel model = new AuctionDetailViewModel();
             model.Auction = AuctionService.Instance.AuctionGetById(id);
            model.PageTitle = "Auction Detail" + model.Auction.Title;
            model.PageDescription = model.Auction.Description.Substring(0,10);

            return View(model);
        }
    }
}