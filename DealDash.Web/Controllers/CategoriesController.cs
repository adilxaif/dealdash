﻿using DealDash.Entities;
using DealDash.Services;
using DealDash.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DealDash.Web.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: Auction
        public ActionResult Index()
        {
            CategoriesListingViewModel model = new CategoriesListingViewModel();
            model.PageTitle = "Category";
            model.PageDescription = "This is Category Page";           
           return View(model);
                 
        }
        // GET: Auction
        public ActionResult Listing()
        {
            CategoriesListingViewModel model = new CategoriesListingViewModel();
            model.Categories = CategoriesService.Instance.GetAllCategory();
            return PartialView(model);
            
          
        }

        public ActionResult Create()
        {
            CategoryViewModel model = new CategoryViewModel();
            
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Create(CategoryViewModel model)
        {
            Category category = new Category();
            category.Name = model.Name;
            category.Description = model.Description;
           
            CategoriesService.Instance.SaveCategory(category);
            return RedirectToAction("Listing");
        }

        public ActionResult Edit(int ID)
        {
            CategoryViewModel model = new CategoryViewModel();
       
            var category = CategoriesService.Instance.CategoryGetById(ID);
            model.ID = category.ID;
            model.Name = category.Name;
          
            model.Description = category.Description;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Edit(CategoryViewModel model)
        {
            Category category =new Category();
            category.ID = model.ID;
            category.Name = model.Name;
            category.Description = category.Description;
            CategoriesService.Instance.UpdateCategory(category);
            return RedirectToAction("Listing");
         
        }

        public ActionResult Delete(int ID)
        {
            var model = CategoriesService.Instance.CategoryGetById(ID);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Delete(Category category)
        {
            CategoriesService.Instance.CategoryDelete(category);
            return RedirectToAction("Listing");
        }

        
        public ActionResult Details(int id)
        {
            CategoriesDetailViewModel model = new CategoriesDetailViewModel();
             model.Categories = CategoriesService.Instance.CategoryGetById(id);
            model.PageTitle = "Auction Detail" + model.Categories.Name;
            model.PageDescription = model.Categories.Description.Substring(0,10);

            return View(model);
        }
    }
}