﻿using DealDash.Services;
using DealDash.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DealDash.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            AuctionViewModel model = new AuctionViewModel();
            model.PageTitle = "Home";
            model.PageDescription = "This is Home Page";
            model.AllAuctions = AuctionService.Instance.GetAllAuctions();
            model.PromotedAuctions = AuctionService.Instance.GetPromotedAuctions();

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}