﻿using DealDash.Database;
using DealDash.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealDash.Services
{
   public class SharedService
    {
        #region SingleTon
        public static SharedService Instance
        {
            get
            {
                if (instance == null) instance = new SharedService();

                return instance;

            }

        }



        private static SharedService instance { get; set; }//is it already save in object if yes then return

        private SharedService()
        {

        }


        #endregion

        public int SaveAuctionPic(Picture picture)
        {
            using (var db = new DealDashContext())
            {
                db.Pictures.Add(picture);
                db.SaveChanges();
                return picture.ID;
            }
        }
    }
}
