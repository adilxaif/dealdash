﻿using DealDash.Database;
using DealDash.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealDash.Services
{
    public class AuctionService
    {

        #region SingleTon
        public static AuctionService Instance
        {
            get
            {
                if (instance == null) instance = new AuctionService();

                return instance;

            }

        }

       

        private static AuctionService instance { get; set; }//is it already save in object if yes then return

        private AuctionService()
        {

        }


        #endregion
        public List<Auction> GetAllAuctions()
        {
            var db = new DealDashContext();
            

                return db.Auctions.ToList();

            
        }

        public List<Auction> GetPromotedAuctions()
        {

            var db = new DealDashContext();
            return db.Auctions.ToList();


        }
        public List<Auction> SearchAuction(int? CategoryID,string searchTerm,int?pageNO,int pageSize)
        {

            var db = new DealDashContext();
            var auctions = db.Auctions.AsQueryable();
            if(CategoryID.HasValue && CategoryID.Value > 0)
            {
                auctions = auctions.Where(x => x.CategoryID == CategoryID.Value);
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                auctions = auctions.Where(x => x.Title.ToLower().Contains(searchTerm.ToLower()));
            }
            pageNO = pageNO ?? 1;
            // pageNO = pageNO.HasValue ? pageNO.Value : 1;
            var skipCount = (pageNO.Value - 1) * pageSize;
            return auctions.OrderByDescending(x=>x.CategoryID).Skip(skipCount).Take(pageSize).ToList();


        }

        public int getAuctionCount(int? CategoryID, string searchTerm)
        {
            var db = new DealDashContext();
            var auctions = db.Auctions.AsQueryable();
            if (CategoryID.HasValue && CategoryID.Value > 0)
            {
                auctions = auctions.Where(x => x.CategoryID == CategoryID.Value);
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                auctions = auctions.Where(x => x.Title.ToLower().Contains(searchTerm.ToLower()));
            }

            return auctions.Count();
        }

        public Auction AuctionGetById(int id)
        {

            var db = new DealDashContext();
            return db.Auctions.Find(id);
             
            
        }

        public void UpdateAuction(Auction auction)
        {
            var db = new DealDashContext();
            var existingAuctions = db.Auctions.Find(auction.ID);
            db.AuctionPictures.RemoveRange(existingAuctions.AuctionPictures);//reomve exiting pic
            db.Entry(existingAuctions).CurrentValues.SetValues(auction);//update auction
            db.AuctionPictures.AddRange(auction.AuctionPictures);
           
             db.SaveChanges();
            
        }

        public void AuctionDelete(Auction auction)
        {
            using (var db = new DealDashContext())
            {
                db.Entry(auction).State = System.Data.Entity.EntityState.Deleted;
              //  db.Auctions.Remove(auction);
                db.SaveChanges();
            }
        }

        public void SaveAuction(Auction auction)
        {
            using (var db = new DealDashContext())
            {
                db.Auctions.Add(auction);
                db.SaveChanges();
            }
        }
    }
}
