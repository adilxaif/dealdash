﻿using DealDash.Database;
using DealDash.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealDash.Services
{
    public class CategoriesService
    {

        #region SingleTon
        public static CategoriesService Instance
        {
            get
            {
                if (instance == null) instance = new CategoriesService();

                return instance;

            }

        }

       

        private static CategoriesService instance { get; set; }//is it already save in object if yes then return

        private CategoriesService()
        {

        }


        #endregion
        public List<Category> GetAllCategory()
        {
            var db = new DealDashContext();
            

                return db.Categories.ToList();

            
        }

        public Category CategoryGetById(int id)
        {

            var db = new DealDashContext();
            return db.Categories.Find(id);
             
            
        }

        public void UpdateCategory(Category category)
        {
            using (var db = new DealDashContext())
            {
                db.Entry(category).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void CategoryDelete(Category category)
        {
            using (var db = new DealDashContext())
            {
                db.Entry(category).State = System.Data.Entity.EntityState.Deleted;
              //  db.Auctions.Remove(auction);
                db.SaveChanges();
            }
        }

        public void SaveCategory(Category category)
        {
            using (var db = new DealDashContext())
            {
                db.Categories.Add(category);
                db.SaveChanges();
            }
        }
    }
}
